// No. 1
console.log("=====No. 1=====");

var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function arrayToObject(arr) {
    var object = {};
    for (var i = 0 ; i < arr.length ; i++){
        
        var umur = ""
        if(arr[i][3]>thisYear || !arr[i][3]){
            umur = "Invalid birth year"
        }else{
            umur = thisYear - arr[i][3]
        }
        var data = {}
        data['firstName'] = arr[i][0],     
        data['lastName'] = arr[i][1], 
        data['gender'] = arr[i][2], 
        data['age'] = umur
        object[(i+1)+". "+arr[i][0]+" "+arr[i][1]] = data
    }
    return console.log(object);
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

console.log("");
// No. 2
console.log("=====No. 2=====");

function shoppingTime(memberId, money) {
    // you can only write your code here!
    var object = {}
    var harga = [1500000, 500000, 250000, 175000, 50000]
    var list = {
        1500000 : "Sepatu Stacattu",
        500000 : "Baju Zoro",
        250000 :'Baju H&N',
        175000 :"Sweater Uniklooh",
        50000 : "Casing Handphone"
    }
    var cart = []
    if (!memberId) {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    } else if (money < 50000) {
        return 'Mohon maaf, uang tidak cukup'
    }
    var sisa = money
    for (var i = 0; i < harga.length; i++) {
        if (money > harga[i]) {
            object['memberId'] = memberId
            object['money'] = money
            cart.push(list[harga[i]])
            object['listPurchased'] = cart
            sisa -= harga[i]
            object['changeMoney'] = sisa
        }
    }
    return object
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("");
// No. 3
console.log("=====No. 3=====");

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var hasil = []
    for (var i = 0 ; i < arrPenumpang.length ; i++){
        var naikDari = arrPenumpang[i][1]
        var tujuan = arrPenumpang[i][2]
        for (let i = 0; i < rute.length; i++) {
            if(naikDari == rute[i]){
                var indexNaik = i
                var naikDari = rute[i]
            }
            if(tujuan == rute[i]){
                var indexTujuan = i
                var tujuan = rute[i]
            }  
        }
        hasil.push({
            'penumpang' : arrPenumpang[i][0],
            'naikDari' : naikDari,
            'tujuan' : tujuan,
            'bayar': Math.abs(indexNaik-indexTujuan)*2000
        })

    }
    return hasil
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]
