// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
readBooks(10000, books[0],function(result){
    readBooks(result,books[1],function(result){ 
        readBooks(result,books[2],function(result){
            readBooks(result,books[0],function(result){})
        })
    })
});