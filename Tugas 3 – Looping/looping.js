// No. 1
console.log('=====No. 1=====');

var deret = 0;
console.log('LOOPING PERTAMA');
while(deret < 20){
    deret += 2;
    console.log(deret, "- I love coding");    
}
console.log('LOOPING KEDUA');
while(deret > 0){
    console.log(deret, "- I will become a mobile developer"); 
    deret -= 2;   
}
console.log('');

// No. 2
console.log('=====No. 2=====');

var text = '';
for(var deret2 = 1; deret2 <= 20; deret2++) {
    if(deret2 % 3 == 0 && deret2 % 2 != 0){
        text = 'I Love Coding';
    }else if(deret2 % 2 == 0){
        text = 'Berkualitas';
    }else{
        text = 'Santai';
    }
    console.log(deret2, '-', text);
  } 
console.log('');

// No. 3
console.log('=====No. 3=====');

for (var j = 1 ; j <=4 ; j++){
    var pagar = ''
    for (var i = 1 ; i <=8 ; i++){
        pagar += '#';   
    }
    console.log(pagar);
}  
console.log('');

// No. 4
console.log('=====No. 4=====');
var pagar = ''
for (var i = 1 ; i <=7 ; i++){
    pagar += '#';
    console.log(pagar);   
}
console.log('');

// No. 5
console.log('=====No. 5=====');
var catur = ''
for (var j = 1 ; j <=8 ; j++){
    if (j>1) {
        catur += "\n"
    }
    for (var i = 1 ; i <= 8 ; i++){ 
        if((j+i) % 2 == 0){
            catur += " ";
        }else{
            catur += "#";
        }
    }
}  
console.log(catur);