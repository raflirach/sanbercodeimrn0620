import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";

import YoutubeUI from "./Tugas12/App.js";
import Tugas13 from "./Tugas13/App";
import Tugas14 from "./Tugas14/App";
import Tugas15 from "./Tugas15/index";
import TugasNav from "./Tugas15/TugasNavigation/index";
import Quis3 from "./Quiz3/index";
import Latihan from "./Latihan/Index";

const App = () => {
  return (
    //<YoutubeUI></YoutubeUI>
    //<Tugas13 />
    //<Tugas14 />
    //<Tugas15 />
    //<TugasNav />
    //<Quis3 />
    <Latihan />
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});

export default App;
