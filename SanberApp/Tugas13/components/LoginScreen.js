import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput } from 'react-native';

export default class VideoItem extends Component {
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Image source={require("./../images/logo.png")} style={{height:100, width:300 }} />
                </View>
                <Text style={styles.title}>Login</Text>
                <View style={styles.formContainer}>
                    <View style={styles.form}>
                        <Text style={styles.label}>Username/ Email</Text>
                        <TextInput style={styles.txtInput} placeholder="username/ email" />
                    </View>
                    <View style={styles.form}>
                        <Text style={styles.label}>Password</Text>
                        <TextInput style={styles.txtInput} placeholder="password" secureTextEntry={true} />
                    </View>
                </View>
                <View style={styles.bottom}>
                    <TouchableOpacity style ={styles.btnMasuk} onPress={() => this.props.navigation.navigate('About')}>
                        <Text style={styles.txtButton}>Masuk</Text>
                    </TouchableOpacity>
                    
                    <Text style={{fontSize:18, color:'#3EC6FF'}}>atau</Text>

                    <TouchableOpacity style ={styles.btnDaftar} onPress={() => this.props.navigation.navigate('Register')}>
                        <Text style={styles.txtButton}>Daftar ?</Text>
                    </TouchableOpacity>
                    
                    
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      backgroundColor: 'white'
    },
    logo:{
        marginVertical: 30,
        alignItems: 'center'
    },
    title:{
        textAlign: 'center',
        fontSize: 24,
        color: '#003366'
    },
    formContainer:{
        marginVertical: 20,
        alignItems: 'center'
    },
    form:{
        width: '70%',
        marginVertical: 5
    },
    label:{
        color: '#003366'
    },
    txtInput:{
        height: 40,
        paddingLeft: 8,
        borderColor: '#003366',
        borderWidth: 1,
    },
    bottom:{
        flexDirection: "column",
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    btnDaftar:{
        marginVertical: 7,
        flexDirection: 'column',
        alignItems: 'center',
        color: 'white',
        width: 120,
        borderRadius: 15,
        backgroundColor : '#003366',
    },
    btnMasuk:{
        marginVertical: 7,
        flexDirection: 'column',
        alignItems: 'center',
        color: 'white',
        width: 120,
        borderRadius: 15,
        backgroundColor : '#3EC6FF',
    },
    txtButton:{
        color:'white', 
        marginVertical: 7, 
        fontSize: 18
    }
  });