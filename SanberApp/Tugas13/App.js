import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from "react-navigation";

import RegisterScreen from './components/RegisterScreen'
import LoginScreen from './components/LoginScreen'
import AboutScreen from './components/AboutScreen'

const App = () => {
  return (
      <AppContainer />    
  );
}

const AppNavigator = createStackNavigator({
    Login: {
      screen: LoginScreen
    },
    Register: {
      screen: RegisterScreen
    },
    About:{
      screen: AboutScreen
    }
  });

const AppContainer = createAppContainer(AppNavigator);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App